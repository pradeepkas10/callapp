//
//  SignUpDetailsViewController.swift
//  CallApp
//
//  Created by Wemonde on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class SignUpDetailsViewController: UIViewController {
    
    @IBOutlet weak var viewCustomPassword:UIView!
    @IBOutlet weak var viewCustomRePassword:UIView!
    @IBOutlet weak var viewCustomUsername:UIView!
    @IBOutlet weak var scrollView:UIScrollView!

    
    var passwordView:OptionsView!
    var rePasswordView:OptionsView!
    var userNameView:OptionsView!

    //MARK:- controller life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDataForViews()
        UIApplication.shared.statusBarView?.backgroundColor = ColorCodes.stausBarColor

    }
    
    
    
    
    //MARK:- button actions

    @IBAction func btnNextAction(sender:UIButton){
        if scrollView.contentOffset.x < screenWidth{
            scrollView.scrollRectToVisible(CGRect.init(x: screenWidth, y: 0, width: screenWidth, height: 250), animated: true)
            return
        }
       else if scrollView.contentOffset.x < 2*screenWidth{
            scrollView.scrollRectToVisible(CGRect.init(x: 2*screenWidth, y: 0, width: screenWidth, height: 250), animated: true)
            return
        }

    }
    
    @IBAction func btnBackAction(sender:UIButton){
        
        if scrollView.contentOffset.x > 2*screenWidth{
            scrollView.scrollRectToVisible(CGRect.init(x: 2*screenWidth, y: 0, width: screenWidth, height: 250), animated: true)
            return
        }
        else if scrollView.contentOffset.x > screenWidth{
            scrollView.scrollRectToVisible(CGRect.init(x: screenWidth, y: 0, width: screenWidth, height: 250), animated: true)
            return
        }
        else if scrollView.contentOffset.x > 0{
            scrollView.scrollRectToVisible(CGRect.init(x: 0, y: 0, width: screenWidth, height: 250), animated: true)
            return
        }
        else{
            self.navigationController?.popToRootViewController(animated: true)
        }

    }
    
    func setUpDataForViews(){
        passwordView = OptionsView.fromNib()
        passwordView.frame = viewCustomPassword.bounds
        viewCustomPassword.addSubview(passwordView)
        passwordView.toAddCondtionalData(viewName: "password")
        
        rePasswordView = OptionsView.fromNib()
        rePasswordView.frame = viewCustomRePassword.bounds
        viewCustomRePassword.addSubview(rePasswordView)
        rePasswordView.toAddCondtionalData(viewName: "re-password")

        userNameView = OptionsView.fromNib()
        userNameView.frame = viewCustomUsername.bounds
        viewCustomUsername.addSubview(userNameView)
        userNameView.toAddCondtionalData(viewName: "name")

    }
    


}
