//
//  ServiceClass.swift
//  JetMate
//
//  Created by cheena
//  Copyright © 2019 Appincuba. All rights reserved.
//

import UIKit
import Alamofire

class ServiceClass: NSObject {

    //MARK: ************* GET INSTANCE *************
    static let sharedInstance = ServiceClass()
    
    
    //MARK: ************* HIT Post  *************
    func hitPostService(url: URL ,params:Dictionary<String,String>,handler:@escaping ((AnyObject?) -> Void)) {
        if networkReachable(){
            //request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: [:]).responseJSON {}
            //reque
            request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                if response.result.isSuccess{
                    handler(response.result.value as AnyObject?)
                }
                else{
                    handler(nil)
                }
            }
        }else{
            AppHelper.showALertWithTag(title: "Zet Mate", message: "Please check your internet connection", delegate: self, cancelButtonTitle: "OK", otherButtonTitle: nil)
        }
    }

    func hitBodyPostService(url: URL ,params:Dictionary<String,Any>,handler:@escaping ((AnyObject?) -> Void)) {
        if networkReachable(){
            //ServiceClass.printRequestURL(url: url, parameters: params)
            print("Request: " ,url,params)
            request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: [:]).responseJSON { (response) in
                
                ServiceClass.printResponse(url: url.absoluteString, response: response)
                //print("Request: " ,url,response.request) // original URL request
                //print(response.response) // URL response
               // print("Response: " ,url,response.response) // original URL request
                //print(response.data) // server data
                //print(response.result) // result of response serialization
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    //
                    print("Data: \(utf8Text)")
                }
                if response.result.isSuccess{

                    handler(response.result.value as AnyObject?)
                } else {
                    handler(nil)
                }
            }
        }else{
            AppHelper.showALertWithTag(title: "Zet Mate", message: "Please check your internet connection", delegate: self, cancelButtonTitle: "OK", otherButtonTitle: nil)
        }
    }

    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }

    //MARK: ************* CHECK NETWORK REACHABILITY *************
    func networkReachable() -> Bool {
        return (NetworkReachabilityManager()?.isReachable)!
    }
    
    func hitMultipartForVideoUploading(imageUrls: [URL], myUrl:String, params:Dictionary<String,String>, handler:@escaping ((AnyObject?) -> Void)) {
        //, docUrls: [String]
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            
            for imageData in imageUrls {
    
                multipartFormData.append(imageData, withName: "passengerIdentityPDF[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        
        }, to: myUrl,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                   
                    //printLogs(value: response)
                    handler(1.0 as AnyObject?)
                    //printLogs(value: "done")
                }
            case .failure(let error):
                //printLogs(value: "failed")
                print(error)
            }
            
        })
    }
    
    func hitMultipartForMakePayment(imageUrls: [URL], myUrl:String, params:Dictionary<String,String>, handler:@escaping ((AnyObject?) -> Void)) {
        //, docUrls: [String]
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            
            for imageData in imageUrls {
                
                multipartFormData.append(imageData, withName: "imagefile", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to: myUrl,
           
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    //printLogs(value: response)
                    handler(1.0 as AnyObject?)
                    //printLogs(value: "done")
                }
            case .failure(let error):
                //printLogs(value: "failed")
                print(error)
            }
            
        })
    }
    
    func hitMultipartForMakePaymentDocUpload(image:UIImage?, myUrl:String, params:Dictionary<String,String>, handler:@escaping ((AnyObject?) -> Void)) {
        
        if networkReachable(){
            upload(multipartFormData: { (multipartFormData) in
                if let img = image {
                    
                    let imgData = img.jpegData(compressionQuality: 0.5)
                    if imgData != nil{
                        multipartFormData.append(imgData!, withName: "imagefile", fileName: "file", mimeType: "image/jpeg")
                    }
                }
                
                for (key, value) in params {
                    
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }, to: myUrl,headers:[:]) { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        //printLogs(value: response)
                        handler(1.0 as AnyObject?)
                        //printLogs(value: "done")
                    }
                case .failure(let error):
                    //printLogs(value: "failed")
                    print(error)
                }
                
            }
        }else{
            //printLogs(value: "failed")
        }
    }
    
    static func printRequestURL(url: String, parameters: [String:  Any]?) {
        print("RequestURL: \(url)")
        
        if let param = parameters {
            do {
                let data = try JSONSerialization.data(withJSONObject: param, options: [])
                if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
                    print("RequestJSON: \(jsonString)")
                }
            } catch {
                print("Exception occurs")
            }
        }
        
        print("\n\n")
    }
    
    static func printResponse(url: String, response: DataResponse<Any>) {
        print("ResponseURL:\(url)")
        switch response.result {
        case .success(let JSON):
            
            if let jsonAttributes = JSON as? [String:Any] {
                do {
                    let data = try JSONSerialization.data(withJSONObject: jsonAttributes, options: [])
                    if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
                        print("ResponseJSON:\(jsonString)")
                    }
                } catch {
                    print("Exception occurs")
                }
            } else if let data = response.data {
                if let jsonString = String(data: data, encoding: String.Encoding.utf8) {
                    print("ResponseString:\(jsonString)")
                }
            } else {
                print("Unknown response")
            }
            break
        case .failure(let error):
            
            print("ResponseError:\(error)")
        }
        print("\n\n")
    }
 }
