//
//  UIViewExtenstions.swift
//  VLCC
//
//  Created by Wemonde on 21/03/18.
//  Copyright © 2017 Wemonde. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable extension UIButton {
    @IBInspectable var cornerRadius :CGFloat {
        
        get {
            return layer.cornerRadius
        }
        
        set {
            
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var cornerRadiusOneSide :CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            let path = UIBezierPath(roundedRect:self.bounds, byRoundingCorners:[.topRight,.bottomRight], cornerRadii: CGSize(width: cornerRadiusOneSide, height: cornerRadiusOneSide))
            let maskLayer = CAShapeLayer()
            
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
            layer.masksToBounds = true
            
        }
    }

}

@IBDesignable extension UIView{

    @IBInspectable var cornerRadiusOfView :CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
}

@IBDesignable extension UIImageView{
    @IBInspectable var cornerRadius :CGFloat{
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
}



@IBDesignable extension UITextField {
    
    @IBInspectable var adjustFontToRealIPhoneSize :Bool{
        get {
            return true
        }
        set {
            
            if adjustFontToRealIPhoneSize {
                let currentFont: UIFont? = font
                var sizeScale: CGFloat = 1.0
                if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6plus {
                    sizeScale = 1.0
                }
                else if IS_IPHONE {
                    sizeScale = 0.9
                }
                
                self.font = currentFont?.withSize(CGFloat(((currentFont?.pointSize)! * sizeScale)))
            }
        }
    
    
    }
}

@IBDesignable extension UILabel {
    
    @IBInspectable var adjustFontToRealIPhoneSize :Bool{
        get {
            return true
        }
        set {
            
            if adjustFontToRealIPhoneSize {
                let currentFont: UIFont? = font
                var sizeScale: CGFloat = 1.0
                if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6plus {
                    sizeScale = 1.1
                }
                else if IS_IPHONE {
                    sizeScale = 0.9
                }
                
                self.font = currentFont?.withSize(CGFloat(((currentFont?.pointSize)! * sizeScale)))
            }
        }
        
        
    }
}


@IBDesignable extension UIButton {
    
    @IBInspectable var adjustFontToRealIPhoneSize :Bool{
        get {
            return true
        }
        set {
            
            if adjustFontToRealIPhoneSize {
                let currentFont: UIFont? = self.titleLabel?.font
                var sizeScale: CGFloat = 1.0
                if IS_IPHONE_6 {
                    sizeScale = 1.0
                }
                else if IS_IPHONE_6plus {
                    sizeScale = 1.1
                }
                else if IS_IPHONE {
                    sizeScale = 0.9
                }
                
                self.titleLabel?.font = currentFont?.withSize(CGFloat(((currentFont?.pointSize)! * sizeScale)))
            }
        }
        
        
    }
  @IBInspectable var bottomBorderUnderButton :UIColor{
    
    get {
        return ColorCodes.textFieldUnderline
    }
    set {

        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottom" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        let bottomBorder:CALayer = CALayer()
        bottomBorder.frame = CGRect(x: 3, y: self.frame.size.height - 5, width: self.frame.size.width - 3, height: 2)
        bottomBorder.backgroundColor = bottomBorderUnderButton.cgColor
        bottomBorder.name = "bottom"
        
        self.layer.addSublayer(bottomBorder)
        
        self.layer.masksToBounds = true
        
        
    }
    }
    
}
