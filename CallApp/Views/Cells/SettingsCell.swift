//
//  SettingsCell.swift
//  CallApp
//
//  Created by Ishika Gupta on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    @IBOutlet weak var titleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func setData(data:[String:Any]) {
        
        titleButton.setTitle(data["title"] as? String ?? "", for: .normal)
        if data["callout"] as? String ?? "" == "1" {
            
        } else {
            
        }
    }
}
