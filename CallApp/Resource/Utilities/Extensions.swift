import UIKit


//MARK:- Story Board
extension UIViewController {
    class func instantiateFromStoryboard(_ name: String = "Main") -> Self {
        return instantiateFromStoryboardHelper(name)
    }
    
    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let identifier = String(describing: self)
        let controller = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        return controller
    }
    
    func add(childViewController child: UIViewController, toSubView: UIView?, frame: CGRect) {
        self.addChild(child)
        child.view.frame = frame
        if let subView = toSubView {
            subView.addSubview(child.view)
        } else {
            self.view.addSubview(child.view)
        }
        child.didMove(toParent: self)
    }
}

//MARK:- View
extension UIView {
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient11(colours: colours, locations: nil)
    }
    
    func applyGradient11(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func cornerRadius(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.contentMode        = .scaleAspectFill
        self.clipsToBounds      = true
    }
    
    func roundBorder() {
        self.cornerRadius(radius: self.bounds.size.height / 2)
    }
    
    func drop(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
       // self.layer.shadowOffset         =   CGSize(width: 6.0, height:0.0)
      
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   = UIScreen.main.scale
         self.layer.zPosition = 1
        
    }
    func dropViewData(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
        // self.layer.shadowOffset         =   CGSize(width: 6.0, height:0.0)
        
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   = UIScreen.main.scale
        //self.layer.zPosition = 1
        
    }
    func dropView(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
        self.layer.shadowOffset         =   CGSize(width: -1.0, height:3.0)
        
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   =   UIScreen.main.scale
        self.layer.zPosition            =   1
        
    }
    
    @IBInspectable var addShadow :Bool {
        get {
            
              return true
            
        }
        set {
            if addShadow {
                self.dropView(shadow: HeaderViewShadow.color, shadowOpacity: HeaderViewShadow.opacity, offset: HeaderViewShadow.offset, radius: HeaderViewShadow.radius)
               
                self.layer.zPosition = 1
            }
        }
    }

}

//MARK:- Text Fields
extension UITextField {
    func bottomBorderForOTP(borderColor color:UIColor, borderWidth width:CGFloat) {
        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottom" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        let bottomBorder:CALayer = CALayer()
        bottomBorder.frame = CGRect(x: 3, y: self.frame.size.height - width, width: self.frame.size.width - 3, height: width)
        bottomBorder.backgroundColor = color.cgColor
        bottomBorder.name = "bottom"
        
        self.layer.addSublayer(bottomBorder)
        
        self.layer.masksToBounds = true
        
        
    }
    
    func bottomBorder(borderColor color:UIColor, borderWidth width:CGFloat) {
        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottomTextField" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        let bottomBorder:CALayer = CALayer()
        
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        bottomBorder.backgroundColor = color.cgColor
        bottomBorder.name = "bottomTextField"

        self.layer.addSublayer(bottomBorder)
    }
    
    func addImage(image:UIImage) {
        
        let paddingView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: image.size.width + 22, height: image.size.height))
        let imageView:UIImageView = UIImageView(frame: CGRect(x: 11, y: 0, width: image.size.width, height: image.size.height))
        imageView.image = image;
        paddingView.addSubview(imageView)
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func addImageToRight(image:UIImage) {
        
        let paddingView:UIImageView = UIImageView(frame: CGRect(x: self.frame.width - image.size.width, y: 0, width: image.size.width, height: image.size.height))
        let imageView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        imageView.image = image;
        paddingView.addSubview(imageView)
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func leftPadding(size:CGFloat) {
        let paddingView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func rightPadding(size:CGFloat) {
        let paddingView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: size, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setBorderForDiffColor(){
        
        self.leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftViewMode = .always
        
        self.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: self.frame.height))
        self.rightViewMode = .always
        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottomLine" || layer.name == "leftline" || layer.name == "right"  ||  layer.name == "top"{
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        let topRight = UIColor.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1/0)
        let bottomLeft = UIColor.init(red: 201/255, green: 202/255, blue: 203/255, alpha: 1/0)

        let height:CGFloat = 1.5
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
        bottomLine.backgroundColor = bottomLeft.cgColor
        bottomLine.name = "bottomLine"
        self.layer.addSublayer(bottomLine)
        
        let leftline = CALayer()
        leftline.frame = CGRect(x: 0, y: 0, width: height, height: self.frame.height)
        leftline.backgroundColor = bottomLeft.cgColor
        leftline.name = "leftline"
        self.layer.addSublayer(leftline)
        
        let right = CALayer()
        right.frame = CGRect(x: self.frame.width-height, y: 0, width: height, height: self.frame.height)
        right.backgroundColor = topRight.cgColor
        right.name = "right"
        self.layer.addSublayer(right)
        
        let top = CALayer()
        top.frame = CGRect(x: 0, y: 0, width: self.frame.width, height:height)
        top.backgroundColor = topRight.cgColor
        top.name = "top"

        self.layer.addSublayer(top)
        
        
    }

    
    
}

//MARK:- UIColor
extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            } else if hexColor.count == 6 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(1.0)
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

//MARK:- UIImage
extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

//MARK:- String
extension String {
    
    var digitsOnly: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }

    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
//    subscript (r: Range<Int>) -> String {
//        let start = index(startIndex, offsetBy: r.lowerBound)
//        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
//        return String(self[Range(start ..< end)])
//    }

    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
    
    func matches(for regex: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let string = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: string.length))
            return results.map { string.substring(with: $0.range)}
        } catch let error {
            print_debug(object: "invalid regex: \(error.localizedDescription)")
            return []
        }
    }
  
        func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
            let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
            let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
            
            return boundingBox.height
        }
        
        func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
            let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
            let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
            
            return boundingBox.width
        }
    func toPhoneNumberUSFormat() -> String {
        return replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
    }
    
    func startsWith(string: String) -> Bool {
        
        if self.hasPrefix(string) { // true
             return true
        }
        
        return false
    }
   

}

//MARK:- Date
extension Date {
    
    func isGreaterThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: Date) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    /// Returns the amount of years from another date
    func dateBefore(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: -years, to: self)!
    }
    
    func dateAfter(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}

//MARK:- Image View
public extension UIImageView {
    public func loadGif(name: String) {
        
//        let imageView = self
//        DispatchQueue.global().async {
//            let image = UIImage.gif(name: name)
//            DispatchQueue.main.async {
//                imageView.image = image
//            }
//        }
    }
    
    func setImagewithCache(url:String, placehlderImage:String)  {
        
//        let urlNew:String = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
//        if let url = URL(string: urlNew) {
//            if urlNew != ""{
//            self.af_setImage(withURL: url, placeholderImage: UIImage(named:placehlderImage) )
//            }
//            else{
//            self.image =  UIImage(named:placehlderImage)
//            }
//        }else{
//        self.image =  UIImage(named:placehlderImage)
//        }
    }
    
    func setImagewithCache(url:String, placehlderImageView:UIImage)  {
        
//        let urlNew:String = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
//        if let url = URL(string: urlNew) {
//            af_setImage(withURL: url, placeholderImage: placehlderImageView )
//        } else {
//            image = placehlderImageView
//        }
    }
    
    func setImagewithCache(url:String, placehlderImageView:UIImage, completionHandler: @escaping () -> ()?)  {
        
//        let urlNew:String = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
//        
//        if let url = URL(string: urlNew) {
//            af_setImage(withURL: url, placeholderImage: placehlderImageView, filter: nil, progress: nil, progressQueue: .main, imageTransition: .crossDissolve(0.2), runImageTransitionIfCached: false, completion: { (response) in
//                completionHandler()
//            })
//        } else {
//            self.image = placehlderImageView
//            completionHandler()
//        }
    }
    
}

//MARK:- Device Type
public extension UIDevice {
    //      Usage:
    //      let modelName = UIDevice.current.modelName

    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

extension UIImage {
    
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == .up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2))
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi/2))
            break
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: (self.cgImage?.colorSpace)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            break
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}


extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func trim() -> String
        {
            return self.trimmingCharacters(in: CharacterSet.whitespaces)
        }
    
}

//MARK:- NSLayoutConstraint
extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}


public extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
}


