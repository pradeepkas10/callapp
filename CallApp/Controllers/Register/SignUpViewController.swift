//
//  SignUpViewController.swift
//  CallApp
//
//  Created by Wemonde on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var btnFacebook:UIButton!
    @IBOutlet weak var btnGoogle:UIButton!
    @IBOutlet weak var txtEmail:CustomTextField!
    @IBOutlet weak var viewCustom:UIView!
    var optionView:OptionsView!

    //MARK:- controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarView?.backgroundColor = ColorCodes.stausBarColor

        optionView = OptionsView.fromNib()
        optionView.frame = viewCustom.bounds
        viewCustom .addSubview(optionView)
        optionView.toAddCondtionalData(viewName: "email")
    }
    
    
    //MARK:- button actions

    @IBAction func btnNextAction(sender:UIButton){
        let vc = SignUpDetailsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnAlreadyAccount(sender:UIButton){
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}


