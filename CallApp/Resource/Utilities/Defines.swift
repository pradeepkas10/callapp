//
//  Defines.swift
//VLCC
//
//  Created by Wemonde on 21/03/18.
//  Copyright © 2017 Wemonde. All rights reserved.
//

import Foundation
import UIKit

let screen          = UIScreen.main.bounds
let screenWidth     = UIScreen.main.bounds.width
let screenHeight    = UIScreen.main.bounds.height

let IS_IPHONE_6plus     = UIScreen.main.bounds.size.height > 667.0
let IS_IPHONE_6         = UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE_5         = UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_4         = UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE           = UIScreen.main.bounds.size.height < 667.0


let appdelgate = UIApplication.shared.delegate as! AppDelegate
let device_UDID = UIDevice.current.identifierForVendor!.uuidString
let kAV = "1.0" ///"6.8"
let kPt = "IOS"
let appName = "Analytics"


let CAFETA = UIFont(name: "cafeta", size: 15)

let HELVETICA_NEUE_Regular = UIFont(name: "Helvetica Neue", size: 15)

let HELVETICA_NEUE_1 = UIFont(name: "Helvetica Neue", size: 28)
let HELVETICA_NEUE_2 = UIFont(name: "Helvetica Neue", size: 13)
let HELVETICA_NEUE_22 = UIFont(name: "Helvetica Neue", size: 22)


let mainStoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
let mainnStoryBoard = UIStoryboard.init(name: "Mainn", bundle: nil)

// MARK : GLOBAL Functions
func print_debug <T> (object: T) {
    print(object)
}


///This Struct is for Color Codes Used in the app. All color code used in the app should be here 243,126,31
struct ColorCodes {
    
    static let stausBarColor =   UIColor.init(red: 228/255, green: 228/255, blue: 228/255, alpha: 1.0)
    
    static let appTheme =  UIColor(red: 243/255.0, green: 150/255.0, blue: 70/255.0, alpha: 1.0)
    static let appTheme1  =   UIColor(red: 215/255.0, green: 125/255.0, blue: 65/255.0, alpha: 1.0)
    
    static let appThemeYellow  =    UIColor(red: 254/255.0, green: 202/255.0, blue: 45/255.0, alpha: 1.0)
    
    static let whiteColor       =   UIColor.white
    static let backgroundColor  =   UIColor(red: 251/255.0, green: 251/255.0, blue: 251/255.0, alpha: 1.0)
    static let greyColor        =   UIColor(red: 146/255.0, green: 146/255.0, blue: 146/255.0, alpha: 1)
    static let darkGreyColor    =   UIColor(red: 64/255.0, green: 64/255.0, blue: 64/255.0, alpha: 1)
    static let redColor         =   UIColor(red: 218/255.0, green: 46/255.0, blue: 56/255.0, alpha: 1)
    static let textFieldUnderline = UIColor(red: 224/255.0, green: 224/255.0, blue: 224/255.0, alpha: 1)
    static let textFieldUnderlineCountry = UIColor(red: 241/255.0, green: 241/255.0, blue: 241/255.0, alpha: 1)
    
    static let addressSection   =   UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1)
    static let textGrayColor    =   UIColor(red: 164/255.0, green: 164/255.0, blue: 164/255.0, alpha: 1)
    static let pickerBackgroundColor   = UIColor(red: 226/255.0, green: 226/255.0, blue: 226/255.0, alpha: 1)
    static let alertViewBlueColor   = UIColor(red: 52/255.0, green: 136/255.0, blue: 250/255.0, alpha: 1)
    static let seachBarColor   =  UIColor(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1)
    static let settingsHeaderColor        =   UIColor(red: 90/255.0, green: 90/255.0, blue: 95/255.0, alpha: 1)
    static let tableBackgroundColor     =   UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1)
    static let previewDefaultBackgroundColor     =   UIColor(red: 167/255.0, green: 167/255.0, blue: 167/255.0, alpha: 1)
    static let previewDefaultLabelColor     =   UIColor.white
    
    static let textFieldTextColor   =   UIColor(red: 62/255.0, green: 62/255.0, blue: 62/255.0, alpha: 1)
    static let textFieldPlaceholderTextColor   =   UIColor(red: 167/255.0, green: 167/255.0, blue: 167/255.0, alpha: 1)
    
    
    
}

struct resultCheckingApi {

    static let success = "success"
    
}

/// This Struct is for Header View Shadow
struct HeaderViewShadow {
    static let color: UIColor   = UIColor.black
    static let radius: CGFloat  = 8
    static let offset: CGSize   =  CGSize.init(width: 10, height: 10)//CGSize.zero
    static let opacity: Float   = 0.5
}

/// This Struct is for Service URLS and BaseUrl. All the service Urls Should be here

struct ServiceURLs {
    
}

struct ServiceMethods{

    static let login                        = "user/login"
    
    
    

}

/// This structer is used for all the Web Links
struct WebLinks {
    static let youtube    =   "https://www.youtube.com/channel/UCYdXIdZg3js_Y1C3SuePriQ"
}


///This structer is used for all the keys used in service class.All the keys used in service class should be here.
struct ServiceKeys {
    static let UserId       = "id"
    static let phoneNumber  = "phoneNumber"
    static let countryCode  = "countryCode"
    static let deviceType   = "deviceType"
   
}

/// This structer is used for all the Segue's in the app
struct Segue {
    static let phoneNumberToCountryList     =   "phoneToCountryList"
    static let phoneToOTP                   =   "phoneToOTP"
    static let otpToRegister                =   "otpToRegister"
   
    
}


/// This structer is used for all the callIdentifiers of tableView or collectionview in app
struct CellIdentifier {
    
    static let contactSegment       =   "ContactSegment"
    
}

/// This structer is used for all the error messages in the app
struct ErrorMessages {
    static let error_SomeThingWentWrong     =   "Something went wrong! Please \n try again."
    static let error_ValidMobileNumber      =   "Please enter a valid mobile number."
    static let error_ValidOTP               =   "Please enter a valid Code."
    static let error_mendatryFields         =   "Looks like you forgot something."
    static let error_Invalid_Email          =   "Please enter a valid email address."
    static let error_No_Camera              =   "Sorry, this device has no camera."
    static let error_InternetCheck          =   "Please check your connection."
    static let error_sameNumber             =   "Old and new numbers cannot be same."
    static let error_OTP                    =   "Please enter a valid OTP."
    static let error_Mobile                 =   "Please enter a valid Mobile Number."
    
    static let error_Exists                 =   "This number is associated with\n another account."
    

}

/// This structer is used for all the Alerts Title's
struct AlertViewTitle {
    static let title_Error      =   "Error!"
    static let title_Success    =   "Success!"
}

/// This structer is used for all the Alert's Button Title
struct AlertViewButtonTitle {
    static let title_Cancel     =   "Cancel"
    static let title_OK         =   "Okay"
    static let title_Done       =   "Done"
    static let title_Camera     =   "Camera"
    static let title_Gallery    =   "Photo Library"
    static let title_NoCamera   =   "No Camera"
    static let numberChangedOK  =   "OK"
    static let title_OKDelete   =   "Yes, delete my account"
    static let title_CancelDelete  =   "No. this was accident"
}

/// This structer is used for all the Alert messages
struct AlertViewMessage {
    static let numberChangedMessage1    =   "\nYour account is now linked to the new number, "
    static let numberChangedMessage2    =   "All your account information can now be accessed at the new number."
    static let numberChangedMessage3    =   "If this was done accidentally, and If you need to link the old number, try after 24 hours."
    static let multipleContactsPicker   =   "Choose one of the following"
    static let countrySuccessfullySuggested  =  "We are working on bringing\n our service to you."
    
    static let profileUpdated           =   "Profile successfully updated."
    static let numberChanged            =   "Number has been changed"
    static let searchRequestSendTitle   =   "Request Sent!"
    static let deleteMediaTitle         =   "Do you really want to delete?"
    static let deleteMediaMessage       =   "You cannot undo this action"
    static let deleteAccountTitle       =   "Do you want to delete\n your account?"
    static let deleteAccountMessage     =   "Deleting the account will remove\n all the information related to\n your account from the phone\n and you will not able to use\n the gifts received."
     static let deleteAccountNote     =   "Note: This cannot be undone."
    static let deleteFavoriteListTitle  =   "Do you really want to delete this store?"
    static let deleteFavoriteListMessage =   "You cannot undo this action"
    static let emptyBarChartsMsg =   "There is no Data."

}

/// This structer is used for all the Mail Composer Text messages
struct MailComposerText {
    static let feedbackSubject  =   "Hello!"
}

/// This structer is used for all the Constant Values used in the app
struct ConstantValues {
    static let iphone   =   "iPhone"
    static let dropGiftTextViewPlaceHolder = "\n \n \n \n            Tap to show your \n            literary genius"
    static let defaultCity = "Tap To select City"
}

/// This structer is used for all the Date Formats Values used in the app
struct DateFormats {
    static let isoFormat        =   "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
    static let isoOutPutFormat        =    "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
    static let dateOfBirth      =   "MMM dd, yyyy"
    static let dropGiftDateFormat         =   "MMM d, yyyy      hh:mma"
}

/// This structer is used for all the UserDefault Key used in the app
struct UserDefaultKeys {
    static let userId           = "userId"
    static let phoneNumber      = "phoneNumber"
    static let userInitialData  = "userInitialData"
    static let userData         = "userData"
    static let userFirstTimeInApp  = "userFirstTimeInApp"
    static let DeviceToken  = "DeviceToken"
     static let notification  = "Notifications"
    static let contacts  = "contacts"
    static let location  = "Notifications"
    static let camera  = "camera"
    static let gallery  = "gallery"
    static let microPhone  = "microPhone"
      static let accessToken  = "accessToken"
    static let CinemaID  = "cinemaID"
    static let CinemaName  = "cinemaName"

    static let CurrentMobileNumber  = "currentMobileNumber"

}

struct EmptyDataSetTitle {
    static let featuredLocationTitle   =  "No Featured Location"
    
   static let selectLocation   = "Select a Location"
     static let wishListTitle          =  "No Wishlist"
    static let countrySelected         =  "No Country"
    static let catagoriesTitle        = "No Catagories"
    static let recentPurchaseSubitle        = "No Recent Purchases"
}
struct EmptyDataSetSubTitle {
     static let featuredLocationSubTitle   =  "We will be in your area soon."
    static let featuredLocationSubTitleForLocation   =  "Please turn on your location, to view featured stores in your location."

    
     
}
