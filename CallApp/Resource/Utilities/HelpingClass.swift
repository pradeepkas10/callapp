//
//  HelpingClass.swift
//  VLCC

//
//  Created by Wemonde on 21/03/18.
//  Copyright © 2017 Wemonde. All rights reserved.
//

import MobileCoreServices
import AVFoundation
import Foundation
import Photos
import UIKit
import Foundation
import AssetsLibrary
import Contacts
import StoreKit
import SafariServices

class HelpingClass: NSObject  {
    // Shared instance
    static let sharedInstance = HelpingClass()
    
    var cityList = [String]()
    var CurrentLoaction:String = ""
    let imagePicker:UIImagePickerController = UIImagePickerController()
    
    var userCityName: String?
    var city: String?
    
    
    var selectedPhoneContacts = [String: [String: String]]()
    var selectedPhoneCNContact = [CNContact]()
    var mediaFile : AnyObject?
    var storeName : String?
    var storePrice : String?
    var nameString : String?
    var dateString : String?
    var nowSelected : Bool?
    var isAmountSelected : Bool?
    var commaSeparatedUserSelectedMobileNumbers: String = ""
    var messageSelected : String?
    var storeBackGroundColor: String?
    var storeFontColor: String?
    var storeIcon: String?
    var mediaDeleteButtonRightConstraintConstant: CGFloat?
    var mediaDeleteButtonTopConstraintConstant: CGFloat?

    fileprivate override init() {
        // Code to be executed once
        
        
    }
    
    
    
    func EmptyMessage(message:String, tableView:UITableView) {
        let rect = CGRect(origin: CGPoint(x: 20,y :0), size:CGSize.init(width: screenWidth-80, height: 80))
        print("rect for labell = = = = \(rect)")
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = AlertViewMessage.emptyBarChartsMsg
        messageLabel.textColor = ColorCodes.appTheme
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.font = HELVETICA_NEUE_Regular
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = .none;
    }
    
    func removeAllUserDefaultsOfDeleteAccount() {
       // userData = nil
        //homeScreenData = nil
        self.removeFromUserDefaultForKey(key: UserDefaultKeys.userId)
        self.removeFromUserDefaultForKey(key: UserDefaultKeys.phoneNumber)
        self.removeFromUserDefaultForKey(key: UserDefaultKeys.userInitialData)
        self.removeFromUserDefaultForKey(key: UserDefaultKeys.userData)
        self.removeFromUserDefaultForKey(key: UserDefaultKeys.accessToken)
        //AppDelegate.sharedInstance().setHomeRootViewController()

    }
    
    //MARK:- URL Requests
    func openLink<VC: UIViewController>(url: String, delegate: VC) where VC: SFSafariViewControllerDelegate {
        if let url = URL(string: url) {
            let svc = SFSafariViewController(url: url)
            if #available(iOS 10.0, *) {
               // svc.preferredControlTintColor = ColorCodes.redColor
            } else {
                // Fallback on earlier versions
            }
            svc.delegate = delegate
            delegate.present(svc, animated: true, completion: { 
                if #available(iOS 10.0, *) {
                   // svc.preferredControlTintColor = ColorCodes.redColor
                } else {
                    // Fallback on earlier versions
                }
            })
        }
    }
    
    //MARK:- Store Review
    func displayReviewController() {
        if #available( iOS 10.3,*){
            SKStoreReviewController.requestReview()
        } else {
            displayWriteReviewDialogue()
        }
    }
    
    
    func displayWriteReviewDialogue() {
        let appStoreLink = "link for app" + "&action=write-review"
        if let appStoreURL = URL(string: appStoreLink) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appStoreURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }


    
    //MARK:- Permissions
    func isUserAuthorizedToAccessGallery() -> Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
    func isUserAuthorizedToNotification() -> Bool {
        return PHPhotoLibrary.authorizationStatus() == .authorized
    }
    
    
    func isUserAuthorizedToUseCamera() -> Bool {
        return AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == .authorized
    }
    
    func isUserAuthorizedToUseMicrophone() -> Bool {
        return AVAudioSession.sharedInstance().recordPermission == AVAudioSession.RecordPermission.granted
    }
    
    //MARK:- Utilities
    func openAppSettings(options: [String : Any], completionHandler completion: ((Bool) -> ())?) {
        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
            // Open Settings
            UIApplication.shared.openURL(settingsURL)
            //  UIApplication.shared.open(, options: [:], completionHandler: nil)
            
            
        }
    }
    
   
    
    //MARK:- Custom Alert
//    func presentCustomAlertinVC<VC: UIViewController>(viewController: VC, title: String, messages: [String], note: String?, confirmButtonTitle: String?, cancelButtonTitle: String?, dismissOnTouch: Bool, identifier: AlertViewIdentifier) where VC:CustomAlertDelegate {
//        let alertVC = CustomAlertViewController.instantiateFromStoryboard("Main")
//        alertVC.delegate = viewController
//        alertVC.dismissOnTouch = dismissOnTouch
//        alertVC.identifier = identifier
//        alertVC.view.isOpaque = false
//        alertVC.modalPresentationStyle = .overCurrentContext
//        alertVC.modalTransitionStyle = .crossDissolve
//        let noteText = note ?? ""
//        let confirmText = confirmButtonTitle ?? ""
//        let cancelText = cancelButtonTitle ?? ""
//        alertVC.configure(withTitle: title, messages: messages, note: noteText, confirmButtonTtile: confirmText, cancelButtonTtile: cancelText)
//        viewController.present(alertVC, animated: true, completion: nil)
//        alertVC.setValues()
//
//    }
    
    //MARK:- Permission View Controller
    // Definition with Delegate : func presentPermissionViewController<VC: UIViewController>(viewController: VC, type: PermissionViewControllerType) where VC:PermissionViewControllerDelegate
//    func presentPermissionViewController(viewController: UIViewController, type: PermissionViewControllerType, canUserSkip: Bool) {
//        let permissionVC = PermissionViewController.instantiateFromStoryboard("Main")
//        permissionVC.type           =   type
//        permissionVC.canUserSkip    =   canUserSkip
//        permissionVC.delegate   = viewController as? PermissionViewControllerDelegate
//        viewController.present(permissionVC, animated: true, completion: nil)
//    }
    
    //MARK:- Feedback View Controller
//    func presentFeedbackViewController(viewController: UIViewController) {
//        let feedbackPopUp = FeedbackViewController.instantiateFromStoryboard("Main")
//        feedbackPopUp.modalPresentationStyle = .overCurrentContext
//        feedbackPopUp.modalTransitionStyle = .crossDissolve
//        viewController.present(feedbackPopUp, animated: true, completion: nil)
//
//    }
    
    //MARK:- Validations
    func isValidEmail(string: String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: string)
    }
    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK:- User Specific
    
    func userFirstTimeInApplication() -> Bool {
        if userDefaultForKey(key: UserDefaultKeys.userFirstTimeInApp) != nil {
            return false
            
        }
        return true
    }
    
    
    //MARK:- User Default
    func saveToUserDefault (value: AnyObject , key: String) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: value)
        let defaults = UserDefaults.standard
        defaults.set(encodedData, forKey: key)
        defaults.synchronize()
    }
    
    // Get from user default
    func stringUserDefaultForKey (key: String) -> String {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key)
        if (value != nil) {
            return value!
        }
        return ""
    }
    
    // Remove from user default
    func removeFromUserDefaultForKey(key: String!) {
        let defaults = UserDefaults.standard
        if UserDefaults.standard.string(forKey: key) != nil {
            defaults.removeObject(forKey: key! as String)
        }
        defaults.synchronize()
    }
    
    func userDefaultForKey(key: String) -> AnyObject? {
        let defaults = UserDefaults.standard
        if let value = defaults.value(forKey: key) {
            let decodedData = NSKeyedUnarchiver.unarchiveObject(with: (value as! Data)) as AnyObject
            return decodedData
        } else {
            return nil
        }
    }
    
    func convert(time : String ,fromFormate:String ,toFormate:String) -> String {
        let dateFormatter = DateFormatter()
        //   dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = fromFormate
        let date = dateFormatter.date(from: time)
        dateFormatter.dateFormat = toFormate
        let result = dateFormatter.string(from: date!)
        return result
    }
    func convert(time : String ,fromFormate:String) -> Date{
        
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = fromFormate
        let date = dateFormatter.date(from: time)
        return date!
        
    }
    func convert(date : Date ,fromFormate:String ) -> String {
        let dateFormatter = DateFormatter()
        // dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = fromFormate
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    
    func makeArrayDataForView(viewName:String)->[String]{
        if (viewName == "email"){
            return ["Valid email.","Not in use."]
        }
        
        if (viewName == "password"){
            return ["At least 6 charactesrs long.", "Contains a letter.", "Contains a number."]
        }
        
        if (viewName == "re-password"){
            return ["Matches new password."]
        }
        
        if (viewName == "name"){
            return ["Username available."]
        }
        
        return [String]()
    }
    
    
}
