//
//  LoginViewController.swift
//  CallApp
//
//  Created by Wemonde on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    //MARK:- controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(red: 224/255, green: 219/255, blue: 166/255, alpha: 1.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    //MARK:- button actions
    @IBAction func btnNoAccount(sender:UIButton){
        for vc in (self.navigationController?.viewControllers)!{
            if vc.isKind(of: SignUpViewController.self) {
                self.navigationController?.popToViewController(vc, animated: true)
                return
            }
        }
        let vc = LoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
