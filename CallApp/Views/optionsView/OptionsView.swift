//
//  OptionsView.swift
//  CallApp
//
//  Created by Wemonde on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class OptionsView: UIView {

    @IBOutlet weak var tblView:UITableView!
    var arrData = [String]()
    var arrCondition = [false,false,false,false,false]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    func toAddCondtionalData(viewName:String){
        tblView.register(UINib.init(nibName: "OptionsTableViewCell", bundle: nil), forCellReuseIdentifier: "OptionsTableViewCell")
        tblView.delegate = self
        tblView.dataSource = self
        arrData = HelpingClass.sharedInstance.makeArrayDataForView(viewName: viewName)
        tblView.reloadData()
    }
    
    
    
}


extension OptionsView :UITableViewDelegate ,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "OptionsTableViewCell", for: indexPath) as? OptionsTableViewCell else{
            return UITableViewCell()
        }
        
        cell.selectionStyle = .none
        cell.lblName.text = arrData[indexPath.item]
        if arrCondition[indexPath.item]{
            cell.imgView.image = UIImage.init(named: "one")
        }else{
            cell.imgView.image = UIImage.init(named: "one")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}
