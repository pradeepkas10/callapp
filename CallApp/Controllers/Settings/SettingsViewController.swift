//
//  SettingsViewController.swift
//  CallApp
//
//  Created by Ishika Gupta on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataArray : [[String:Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initialSetup()
    }
    
    func initialSetup() {
        
        dataArray = [
            ["title":"Profile","callout":"0"],
            ["title":"Privacy Policy","callout":"1"],
            ["title":"Terms and Condition","callout":"1"],
            ["title":"About the App","callout":"1"],
            ["title":"Send Feedback","callout":"0"],
            ["title":"Rate App","callout":"0"],
            ["title":"Share App","callout":"0"],
            ["title":"Logout","callout":"0"]
        ]
        
        self.tableView.reloadData()
        
    }

}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as? SettingsCell
        cell?.setData(data: dataArray[indexPath.row])
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

